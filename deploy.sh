# create a file to test script
touch scriptStart.txt

# Enter the correct folder
cd app/gitlab-ci-introduction

# Pull latest
git pull

# Update the deploy script
cp ./deploy.sh ~/app/deploy.sh

# rebuild
docker-compose up -d --build

# Exit the folder again
cd ../..

# create a file to test script
touch scriptEnd.txt
