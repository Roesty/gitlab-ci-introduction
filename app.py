import time

import redis
from flask import Flask, request
from pymongo import MongoClient 

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

mongo_url = 'mongodb://db:27017'
main_db_name = "mainDB"
col_variables_name = "variables"
col_formulas_name = "formulas"

math_symbols = ["+", "-"]

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)
            
def frame(text):
    count = get_hit_count()
    
    result = ""
    print("+", "-"*(len(text)+2), "+", sep="")
    print("|", text, "|")
    print("+", "-"*(len(text)+2), "+", sep="")
            
def framed():
    count = get_hit_count()
    
    text = 'Hello! This page has been opened {} times.'.format(count)
    
    result = ""

    result += "\n"
    result += "+"+"-"*(len(text)+2)+"+"+"\n"
    result += "| "+text+" |"+"\n"
    result += "+"+"-"*(len(text)+2)+"+"

    count_backup = count

    factorials = list()
    while count_backup > 1:
        for divisor in range(2, count_backup+1):
            if count_backup % divisor == 0:
                factorials.append(divisor)
                count_backup //= divisor
                break

    result += "\n\n"
    result += "{} factorials: ".format(count)
    for factorial in factorials:
        result += "{} * ".format(factorial)

    result = result.strip(" * ")

    result += "\n"

    return result

def solveRec(formula):
    containsSymbol = False
    
    symbol_index = 0
    math_symbol = ""
   
    for index, char in enumerate(formula):
        if char in math_symbols:
            symbol_index = index
            math_symbol = char
            containsSymbol = True
            break
        
    if containsSymbol:
        left = int(formula[:symbol_index])
        right = int(solveRec(formula[symbol_index+1:]))
        
        if char == "+":
            return left + right
        
        if char == "-":
            return left - right
        
    else:
        return formula


@app.route('/')
def hello():
    return framed()

@app.route('/dbnames')
def dbnames():
        
    mongo_client = MongoClient(mongo_url)

    result = "###\n"
    for iii, name in enumerate(mongo_client.list_database_names()):
        result += "{}. {}\n".format(iii, name)
        
    mongo_client.close()

    result += "###"
    return result

@app.route('/test')
def testRoute():
    return "testroute"

@app.route('/purge')
def purge():
    mongo_client = MongoClient(mongo_url)
    database = mongo_client[main_db_name]
    
    for col_name in [col_formulas_name, col_variables_name]:
        collection = database[col_name]
        collection.drop()
    
    mongo_client.close()
    return "purged"

@app.route('/give/var')
def giveVar():
    with MongoClient(mongo_url) as mongo_client:
        
        varName = str(request.form["name"])
        value = str(request.form["value"])
        # value += " - "
        # value += str(get_hit_count())
        
        database = mongo_client[main_db_name]
        
        col_variables = database[col_variables_name]
        
        mydict = {
            "name": varName,
            "value": value
            }
        
        variableExists = False
        
        # Check if this variable already is in the database
        existingDocuments = len(list(col_variables.find({"name": varName})))
        
        message = ""
        
        if existingDocuments:
            x = col_variables.update_one(
                {"name": varName},
                {"$set": {"value": value}}
            )
            message += "Variable '{}' exists {} times. Modified existing.\n".format(varName, existingDocuments)
            
        else:
            x = col_variables.insert_one(mydict)
            message += "Does not exist. Created new.\n".format(existingDocuments)
            
        entryID = str(x)
        
        # message += "Variable '{}' set with value of '{}' and id of '{}'".format(varName, value, entryID)
        
    return message

@app.route('/give/formula')
def giveFormula():
    pass

@app.route('/get/var')
def getVar():
    
    value = 0
    
    with MongoClient(mongo_url) as mongo_client:
            
        varName = str(request.form["name"])
        
        database = mongo_client[main_db_name]
        
        col_variables = database[col_variables_name]
                        
        # Check if this variable already is in the database
        existingDocuments = len(list(col_variables.find({"name": varName})))
        
        message = ""
        
        if existingDocuments:
            
            x = col_variables.find_one({"name": varName})
            
            value = x["value"]
            
            message += "Variable '{}' has value '{}'.\n".format(varName, value)
            
        else:
            message += "Variable '{}' does not exist.\n".format(varName)
        
    return message

@app.route('/get/formula')
def getFormula():
    pass

@app.route('/get/varAll')
def getVarAll():
    pass

@app.route('/get/formulaAll')
def getFormulaAll():
    pass

@app.route('/solve', methods = ['POST'])
def solve():
    formula = request.form["formula"]
    
    message = formula
    
    for math_symbol in math_symbols:
        message = message.replace(math_symbol, " "+math_symbol+" ")
    
    parts = message.split(" ")
    
    message += " = "
    
    missing_variables = False
        
    with MongoClient(mongo_url) as mongo_client:
        
        database = mongo_client[main_db_name]
        
        col_variables = database[col_variables_name]
        
        for part in set(parts):
            if (part not in math_symbols):
                
                result = col_variables.find_one({"name": part})
                
                if not result:
                    message += "\nvariable '{}' does not exist.".format(part)
                    missing_variables = True
                else:
                    formula = formula.replace(part, result["value"])
        
        if not missing_variables:
            message += formula
            message += " = "
            message += '' + str(solveRec(formula))
        
    return message

@app.route('/postTest', methods = ['POST'])
def postTest():
    varName = request.form["instruction"]
    return varName

@app.route('/post', methods = ['POST'])
def post():
    instruction = request.form["instruction"]
    dataType = request.form["dataType"]
       
    if instruction=="give" and dataType=="var":
        return giveVar()
    
    if instruction=="give" and dataType=="formula":
        return giveFormula()
    
    if instruction=="get" and dataType=="var":
        return str(getVar())
    
    if instruction=="get" and dataType=="formula":
        return str(getFormula())
    
    if instruction=="solve":
        return str(solve())
    
    # 3 varName = request.form["instruction"]
    # return varName








