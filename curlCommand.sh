curl -s http://localhost:8000/

echo "\n\nResult of give"
curl -s -F 'instruction=give' -F 'dataType=var' -F "name=x" -F "value=10" http://localhost:8000/post
curl -s -F 'instruction=give' -F 'dataType=var' -F "name=y" -F "value=13" http://localhost:8000/post
curl -s -F 'instruction=give' -F 'dataType=var' -F "name=z" -F "value=120" http://localhost:8000/post

echo "\nResult of get"
curl -s -F 'instruction=get' -F 'dataType=var' -F "name=x" http://localhost:8000/post
curl -s -F 'instruction=get' -F 'dataType=var' -F "name=y" http://localhost:8000/post
curl -s -F 'instruction=get' -F 'dataType=var' -F "name=z" http://localhost:8000/post

# echo "\n\nResult of dbnames"
# curl -s http://localhost:8000/dbnames
echo "\nResult of solve"
curl -s -F "formula=x+y-z" http://localhost:8000/solve

# curl -s http://localhost:8000/purge
